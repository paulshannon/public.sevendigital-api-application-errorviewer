USE [crm]
GO
/****** Object:  Table [dbo].[tblErrors]    Script Date: 10/12/2011 09:41:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrors](
	[errorLogID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dateTime] [datetime] NOT NULL,
	[hostname] [varchar](20) NULL,
	[appId] [varchar](6) NULL,
	[location] [varchar](255) NULL,
	[level] [varchar](10) NULL,
	[message] [nvarchar](1000) NULL,
	[trace] [varchar](2000) NULL,
	[url] [varchar](255) NULL,
	[referrer] [varchar](255) NULL,
	[ip] [varchar](20) NULL,
	[properties] [varchar](255) NULL,
 CONSTRAINT [PK_tblError] PRIMARY KEY CLUSTERED 
(
	[errorLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF