using System;
using System.IO;
using Nancy.Testing;
using Nancy.Testing.Fakes;

namespace SevenDigital.API.Application.ErrorViewer.Acceptance.Tests
{
    public class TestBootstrapper : Bootstrapper
    {
        protected override Type RootPathProvider
        {
            get
            {
                // TODO - figure out a nicer way to do this
                var assemblyPath = Path.GetDirectoryName(typeof(Bootstrapper).Assembly.CodeBase).Replace(@"file:\", string.Empty);
                var rootPath = PathHelper.GetParent(assemblyPath, 3);
                rootPath = Path.Combine(rootPath, @"SevenDigital.API.Application.ErrorViewer");

                FakeRootPathProvider.RootPath = rootPath;
                Console.WriteLine(rootPath);
                return typeof(FakeRootPathProvider);
            }
        }
    }
}