﻿using System;
using NUnit.Framework;
using Nancy;
using Nancy.Testing;

namespace SevenDigital.API.Application.ErrorViewer.Acceptance.Tests
{
    [TestFixture]
    public class EndPointTests
    {
        private Browser _browser;

        [SetUp]
        public void SetUp()
        {
            var bootstrapper = new Bootstrapper();
            _browser = new Browser(bootstrapper);
        }

        [Test]
        public void Should_return_ok_from_root_request()
        {
            var response = _browser.Get("/", (with) => with.HttpRequest());
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void Should_return_ok_from_environment_request()
        {
            var response = _browser.Get("/environment/local/", (with) => with.HttpRequest());
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void Should_return_ok_from_application_request()
        {
            var response = _browser.Get("/environment/local/application/api/", (with) => with.HttpRequest());
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }
    }

    [TestFixture]
    public class RazorRenderingTests
    {
        [Test]
        public void Should_return_html_from_razor()
        {
            var bootstrapper = new TestBootstrapper();
            var browser = new Browser(bootstrapper);
            var result = browser.Get("/", (with) => with.HttpRequest());
            Console.WriteLine(result.Body.AsString());
            result.Body["#container"]
                .ShouldExist();
        }
    }
}
