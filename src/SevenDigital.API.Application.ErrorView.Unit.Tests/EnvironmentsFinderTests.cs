﻿using NUnit.Framework;
using SevenDigital.API.Application.ErrorViewer.Models;
using SevenDigital.API.Application.ErrorViewer.Services;

namespace SevenDigital.API.Application.ErrorViewer.Unit.Tests
{
    [TestFixture]
    public class EnvironmentsFinderTests
    {
        private EnvironmentsModel _result;

        [SetUp]
        public void SetUp()
        {
            var subject = new EnvironmentsFinder();
            _result = subject.FindAll();
        }

        [Test]
        public void Should_create_environments_model_from_request()
        {
            Assert.That(_result, Is.InstanceOf<EnvironmentsModel>());
        }

        [Test]
        [TestCase("local")]
        [TestCase("dev")]
        [TestCase("systest")]
        [TestCase("uat")]
        [TestCase("live")]
        public void Default_list_should_contain(string expectedEnvironment)
        {
            CollectionAssert.Contains(_result, new EnvironmentModel(expectedEnvironment));
        }
    }
}
