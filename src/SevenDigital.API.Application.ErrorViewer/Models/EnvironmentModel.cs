namespace SevenDigital.API.Application.ErrorViewer.Models
{
    public class EnvironmentModel
    {
        private readonly string _name;

        public EnvironmentModel(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }

        public bool Equals(EnvironmentModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._name, _name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (EnvironmentModel)) return false;
            return Equals((EnvironmentModel) obj);
        }

        public override int GetHashCode()
        {
            return (_name != null ? _name.GetHashCode() : 0);
        }
    }
}