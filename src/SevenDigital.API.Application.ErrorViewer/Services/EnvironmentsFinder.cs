using SevenDigital.API.Application.ErrorViewer.Models;

namespace SevenDigital.API.Application.ErrorViewer.Services
{
    public class EnvironmentsFinder
    {
        public EnvironmentsModel FindAll()
        {
            return new EnvironmentsModel
                       {
                           new EnvironmentModel("local"),
                           new EnvironmentModel("dev"),
                           new EnvironmentModel("systest"),
                           new EnvironmentModel("uat"),
                           new EnvironmentModel("live")
                       };
        }
    }
}