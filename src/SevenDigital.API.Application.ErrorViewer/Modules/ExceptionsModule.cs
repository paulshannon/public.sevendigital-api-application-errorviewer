﻿using Nancy;
using System.Linq;

namespace SevenDigital.API.Application.ErrorViewer.Modules
{
    public class ExceptionsModule : NancyModule
    {
        public ExceptionsModule()
            : base("/environment/{Env}/application/{App}")
        {
            Get["/"] = req =>
                           {
                               return "Showing exceptions for " + req.App + " on " + req.Env;
                           };
        }
    }
}
