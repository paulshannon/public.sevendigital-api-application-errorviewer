﻿using Nancy;
using System.Linq;
using SevenDigital.API.Application.ErrorViewer.Services;

namespace SevenDigital.API.Application.ErrorViewer.Modules
{
    public class EnvironmentsModule : NancyModule
    {
        public EnvironmentsModule()
            : base("/")
        {
            Get["/"] = request => View[new EnvironmentsFinder().FindAll()];
        }
    }
}
