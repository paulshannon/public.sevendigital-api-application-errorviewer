﻿using Nancy;

namespace SevenDigital.API.Application.ErrorViewer.Modules
{
    public class ApplicationsModule : NancyModule
    {
        public ApplicationsModule()
            : base("/environment/{Env}/")
        {
            Get["/"] = req => "Showing applications for " + req.Env;
        }
    }
}
